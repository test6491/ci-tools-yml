# ci-tools-yml

This module contains abstract gitlab ci function

## ci-docker

Since 1.0.0

Abstract ci function to push docker image on gitlab ci registry

### Usage

- extend .docker_build on any stage you want to push image
- set IMAGE_NAME, REGISTRY_URL, VERSION and optional DOCKER_BUILD_ARGS variables

```
include:
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/x.y.z/ci-docker.yml

stages:
  - build

variables:
  IMAGE_NAME: $CI_PROJECT_NAME
  REGISTRY_URL: $CI_REGISTRY/$CI_PROJECT_ROOT_NAMESPACE
  DOCKER_BUILD_ARGS: --build-arg CI_K8S_CONFIG="$CI_K8S_CONFIG"

build:
  except:
    - tags
  variables:
    VERSION: $CI_COMMIT_REF_SLUG
  extends: .build

build:tag:
  only:
    - tags
  variables:
    VERSION: $CI_COMMIT_TAG
  extends: .build

.build:
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/docker:latest
  stage: build
  extends: .docker_build
```

## release-common.yml

Since 1.0.0

Abstract ci function to manage release phase with patch, minor or major keyword instead of version

### Usage

release-common must be used with an other release yml (see below)

## release-tools.yml

Since 1.0.0

Abstract ci function to manage release for tools project (project with single project.json with version)

- update version of project.json
- push tag on git
- publish release on gitlab (only source, no artifact)

### Dependencies

- release-common
- a project.json file with version

```
{
  "version": "1.0.0"
}
```

### Usage

- declare 2 stages in this order : prepare_release and release
- create 3 manual job and extends the corresponding release type from release-tools

Warning : only one of thoses 3 jobs must be run for a single pipeline

```
include:
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/1.0.0/release-common.yml
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/1.0.0/release-tools.yml

stages:
  - prepare_release
  - release
  
release:patch:
  extends: .release-tools:patch
  when: manual

release:minor:
  extends: .release-tools:minor
  when: manual

release:major:
  extends: .release-tools:major
  when: manual
```

## pipeline-docker-tools.yml

Since 1.0.0

Complete pipeline for a standard docker tools project

- build and push docker image on gitlab repository (with branche name or tag version as version)
- abstract test job with the pushed docker image
- complete release jobs

### Dependencies

- ci-docker
- release-common
- release-tools
- a project.json file with version

```
{
  "version": "1.0.0"
}
```

### Usage

Exemple for a custom k8s tools image

```
variables:
  DOCKER_BUILD_ARGS: --build-arg CI_K8S_CONFIG="$CI_K8S_CONFIG"

include:
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/x.y.z/ci-docker.yml
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/x.y.z/release-common.yml
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/x.y.z/release-tools.yml
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/x.y.z/pipeline-docker-tools.yml

.test:
  script:
    - kubectl get nodes
```

## sonar-js.yml

Since 1.1.0

Abstract ci function to send js project code and lcov to sonar

### Usage

- extend .sonar-js on any stage you want to send to sonar
- set SONAR_PROJECT_BASEDIR, SONAR_PROJECT_SRC, SONAR_INCLUSIONS, SONAR_EXCLUSIONS, SONAR_TEST_INCLUSIONS, 
  SONAR_TEST_EXCLUSIONS, SONAR_PROJECT_SRC_TEST and SONAR_LCOV_PATH variables
  
```
test:sonar-js:
  stage: test
  variables:
    SONAR_PROJECT_BASEDIR: ./sonar-test
    SONAR_PROJECT_SRC: ./src
    SONAR_INCLUSIONS: "**/*.js"
    SONAR_EXCLUSIONS: "**/node_modules/**/*"
    SONAR_TEST_INCLUSIONS: "**/*.test.js"
    SONAR_TEST_EXCLUSIONS: "**/node_modules/**/*"
    SONAR_PROJECT_SRC_TEST: ./src
    SONAR_LCOV_PATH: ./coverage/lcov.info
  extends: .sonar-js
```

## release-node.yml

Since 1.2.0

Abstract ci function to release node project based on yarn

### Dependencies

- release-common
- a package.json file with version
- run into an image with node and yarn

### Usage

- declare a stages prepare_release and release
- create a job prepare_release extends .prepare_release_node and with your own rules
- create 3 manual job and extends the corresponding release type from release-node


```
include:
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/1.2.0/release-common.yml
  - remote: https://gitlab.com/$CI_PROJECT_ROOT_NAMESPACE/ci-tools-yml/-/raw/1.2.0/release-node.yml

stages:
  - prepare_release
  - release
  
prepare_release:
  extends: .prepare_release_node
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: on_success
      
release:patch:
  extends: .release-node:patch
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual

release:minor:
  extends: .release-node:minor
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual

release:major:
  extends: .release-node:major
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^(main|master|(dev)\/.*)$/'
      when: manual
```